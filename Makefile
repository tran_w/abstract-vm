##
## Makefile for Makefile in /home/naina_r//Malloc
## 
## Made by richard naina
## Login   <naina_r@epitech.net>
## 
## Started on  Sat Feb  2 15:00:21 2013 richard naina
## Last update Sun Feb 24 16:05:58 2013 richard naina
##

NAME	=	avm	

SRCS	=	 src/Int8.cpp \
		 src/Int16.cpp \
		 src/Int32.cpp \
		 src/Double.cpp \
		 src/Float.cpp \
		 src/Exception.cpp \
		 src/OperandCreator.cpp \
		 src/Operand.cpp \
		 src/Type.cpp \
		 src/Calculator.cpp \
		 src/Parser.cpp

OBJS	=	$(SRCS:.cpp=.o)

CXXFLAGS	+=	-W -Wall -Werror -I ./inc

all	:	$(NAME)

$(NAME)	:	$(OBJS)
		$(CXX) -o $(NAME) $(OBJS)

clean	:	
		$(RM) $(OBJS)

fclean	:	clean
		$(RM) $(NAME)

re	:	fclean all

.PHONY	:	all fclean clean re
