#include	<exception>
#include	"Exception.hh"

AbstractVM::Exception::Exception(std::string const& message) throw()
  :  _errorMessage(message)
{
}

AbstractVM::Exception::~Exception() throw()
{
}

const char*             	AbstractVM::Exception::what() const throw()
{
  return (this->_errorMessage.c_str());
}