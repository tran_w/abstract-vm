#include <iostream>
#include "OperandCreator.hh"
#include "Int8.hh"
#include "Int16.hh"
#include "Int32.hh"
#include "Double.hh"
#include "Float.hh"

AbstractVM::Operand::OperandCreator::OperandCreator() 
{
}

AbstractVM::Operand::OperandCreator::~OperandCreator()
{
}

IOperand * AbstractVM::Operand::OperandCreator::createOperand(eOperandType type, const std::string & value)
{
  OperandCreator *p = NULL;
  static std::map<eOperandType, IOperand*(AbstractVM::Operand::OperandCreator::*)(const std::string &)> _operandMap = AbstractVM::Operand::OperandCreator::createMap();
  ptr create;

  create = _operandMap[type];

  return ((p->*create)(value));
}

IOperand*	AbstractVM::Operand::OperandCreator::createInt8(const std::string & value)
{
  return (new AbstractVM::Operand::Int8(value));
}
IOperand*	AbstractVM::Operand::OperandCreator::createInt16(const std::string & value)
{
  return (new AbstractVM::Operand::Int16(value));
}
IOperand*	AbstractVM::Operand::OperandCreator::createInt32(const std::string & value)
{
  return (new AbstractVM::Operand::Int32(value));
}
IOperand*	AbstractVM::Operand::OperandCreator::createFloat(const std::string & value)
{
  return (new AbstractVM::Operand::Float(value));
}
IOperand*	AbstractVM::Operand::OperandCreator::createDouble(const std::string & value)
{
  return (new AbstractVM::Operand::Double(value));
}
