#include			"Calculator.hh"
#include			"Exception.hh"

void				parseFile(std::string filename)
{
  std::ifstream			file;
  std::string			instruct;
  std::string			value;
  AbstractVM::Calculator	op;
  size_t			pos;

  file.open(filename.c_str());
  if (file.is_open() == false)
    throw AbstractVM::Exception("File does not exist");
  while (file.good() == true && file.eof() != true && instruct != ";;")
    {
      getline(file, instruct, '\n');
      if (instruct[0] == ';'){} 
      else if ((pos = instruct.find(' ')) != instruct.npos)
	{
	  value = instruct.substr(pos + 1, instruct.npos - 1);
	  instruct = instruct.substr(0, pos);
	  op.do_instruct(instruct, value); 
	}
      else
	{
	  op.do_instruct(instruct);
	  if (op.getStatus() == true)
	    return ;
	}
    }
  file.close();
  if (instruct != "exit")
    throw AbstractVM::Exception("Last instruction should be exit");
}

void				putIntoFile()
{
  std::string	instruct = "";
  std::ofstream	file;

  file.open("/tmp/.tmpfile");
  while (instruct != ";;")
    {
      getline(std::cin, instruct);
      file << instruct << std::endl;
    }
  file.close();
  parseFile("/tmp/.tmpfile");
}

int		main(int ac, char **av)
{
  if (ac > 1)
    parseFile(av[1]);
  else
    putIntoFile();
}
