#include	"Int16.hh"

AbstractVM::Operand::Int16::Int16(const std::string & value) :
  Operand(INT16, value)
{
}

AbstractVM::Operand::Int16::~Int16()
{
}
