#include	"Int32.hh"

AbstractVM::Operand::Int32::Int32(const std::string & value) : 
  Operand(INT32, value)
{
}

AbstractVM::Operand::Int32::~Int32()
{
}
