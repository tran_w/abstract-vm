#include	"Int8.hh"

AbstractVM::Operand::Int8::Int8(const std::string & value) :
  Operand(INT8, value)
{
}

AbstractVM::Operand::Int8::~Int8()
{
}
