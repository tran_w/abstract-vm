#ifndef     OPERANDCREATOR_HH_
# define    OPERANDCREATOR_HH_

# include   <string>
# include   <map>
# include   "IOperand.hh"

namespace AbstractVM
{
	namespace Operand
	{
		class OperandCreator
		{
			IOperand * createInt8(const std::string & value);
			IOperand * createInt16(const std::string & value);
			IOperand * createInt32(const std::string & value);
			IOperand * createFloat(const std::string & value);
			IOperand * createDouble(const std::string & value);
			static std::map<eOperandType,IOperand*(AbstractVM::Operand::OperandCreator::*)(const std::string &)> createMap()
			{
				std::map<eOperandType,IOperand*(AbstractVM::Operand::OperandCreator::*)(const std::string &)> m;

				m[INT8] = &AbstractVM::Operand::OperandCreator::createInt8;
				m[INT16] = &AbstractVM::Operand::OperandCreator::createInt16;
				m[INT32] = &AbstractVM::Operand::OperandCreator::createInt32;
				m[FLOAT] = &AbstractVM::Operand::OperandCreator::createFloat;
				m[DOUBLE] = &AbstractVM::Operand::OperandCreator::createDouble;

				return m;
			}

		public:
			OperandCreator();
			~OperandCreator();
			static IOperand * createOperand(eOperandType type, const std::string & value);
		};
	}
}

typedef IOperand*(AbstractVM::Operand::OperandCreator::*ptr)(const std::string &);

#endif
