#ifndef				INT8_HH_
# define			INT8_HH_

# include			"Operand.hh"

namespace           AbstractVM
{
    namespace       Operand
    {
        class       Int8 : public Operand<int8_t>
        {
        public:
          Int8(const std::string & value);
          ~Int8();
        };
    }
}

#endif		/* INT8_HH_ */
