#ifndef		OPERAND_HH_
# define	OPERAND_HH_

# include	<stdint.h>

# include	"IOperand.hh"

// This class need to be templated.

namespace		AbstractVM
{
	namespace 	Operand
	{
		template <typename T>
		class 	Operand : public IOperand
		{
		 protected:
			eOperandType	_type;
			std::string 	_str;

		 public:
			 virtual std::string const & toString() const;

			 virtual int getPrecision() const;
			 virtual eOperandType getType() const;

			 virtual IOperand * operator+(const IOperand &rhs) const;
			 virtual IOperand * operator-(const IOperand &rhs) const;
			 virtual IOperand * operator*(const IOperand &rhs) const;
			 virtual IOperand * operator/(const IOperand &rhs) const;
			 virtual IOperand * operator%(const IOperand &rhs) const;

			 Operand(eOperandType type, const std::string & value);
			 virtual ~Operand();
		};
	}
}
#endif
