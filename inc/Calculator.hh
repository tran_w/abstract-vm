#ifndef		CALCULATOR_HH_
# define	CALCULATOR_HH_

# include	<fstream>
# include	<iostream>
# include	<stack>
# include	"Type.hh"

namespace	AbstractVM
{
  class		Calculator
  {
  private:
    std::stack<IOperand *> _mystack;
    std::map<std::string, void (Calculator::*)()> _mymap;
    std::map<std::string, eOperandType> _typemap;
    bool	_exit;
  public:
    Calculator();
    ~Calculator();
    
    bool		getStatus();
    void		setType(AbstractVM::Type *, std::string &);
    void		do_instruct(std::string &);
    void		do_instruct(std::string &, std::string &);
    void		push(IOperand *);
    void		assert(IOperand *);
    void		pop();
    void		dump();
    void		add();
    void		sub();
    void		mul();
    void		div();
    void		mod();
    void		print();
    void		exit();
  };
}

#endif		/* CALCULATOR_HH_ */
