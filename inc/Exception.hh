#ifndef			EXCEPTION_HH_
# define		EXCEPTION_HH_

#include 		<string>
#include 		<exception>

namespace		AbstractVM
{
	class Exception : public std::exception
	{
	public:
	  Exception(std::string const& msg) throw();
	  virtual ~Exception() throw();

	  virtual const char* what() const throw();

	private:
	  std::string   _errorMessage;
	};
}
#endif          /* EXCEPTION_HH_ */
