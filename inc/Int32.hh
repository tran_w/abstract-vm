#ifndef                         INT32_HH_
# define                        INT32_HH_

# include                       "Operand.hh"

namespace                       AbstractVM
{
    namespace                   Operand
    {
        class                   Int32 : public Operand<int32_t>
        {
        public:
          Int32(const std::string & value);
          ~Int32();
        };
    }
}
#endif          /* INT32_HH_ */
