#ifndef				INT16_HH_
# define			INT16_HH_

# include			"Operand.hh"

namespace           AbstractVM
{
    namespace       Operand
    {
        class       Int16 : public Operand<int16_t>
        {
        public:
          Int16(const std::string & value);
           ~Int16();
         };
    }
}

#endif		/* INT16_HH_ */
