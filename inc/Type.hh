#ifndef		TYPE_HH_
# define	TYPE_HH_

# include	<map>
# include	<iostream>
# include	"IOperand.hh"

namespace	AbstractVM
{
  class		Type
  {
  private:
    std::string	_type;
    std::string	_value;
    std::map<std::string , eOperandType> _typemap;
  public:
    Type();
    ~Type();

    std::string		getType() const; 
    std::string		getValue() const;
    eOperandType	getTypemap(std::string);
    void		setType(std::string);
    void		setValue(std::string);
    void		checkType();
    void		checkValue();
  };
}

#endif
