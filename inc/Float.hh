#ifndef			FLOAT_HH_
# define		FLOAT_HH_

# include		"Operand.hh"

namespace		AbstractVM
{
    namespace		Operand
    {
        class		Float : public Operand<float>
        {
        public:
          Float(const std::string & value);
          ~Float();
        };
    }
}

#endif          /* FLOAT_HH_ */
